package Windows;

import java.sql.*;
import java.util.*;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.table.*;

import Objects.*;

public class ThirdWindow extends javax.swing.JFrame {

	private static final long serialVersionUID = 1L;

	public ThirdWindow() {
		initComponents();
		Show_ThirdWindow_In_JTable();
	}

	int pos;

	public Connection getConnection() {
		Connection con = null;
		try {
			String url = "jdbc:mysql://localhost:3306/schooldb?serverTimezone=" + TimeZone.getDefault().getID();
			con = DriverManager.getConnection(url, "root", "dasdasdas2");
			return con;
		} catch (SQLException ex) {
			Logger.getLogger(ThirdWindow.class.getName()).log(Level.SEVERE, null, ex);
			JOptionPane.showMessageDialog(null, "Not Connected");
			return null;
		}

	}

	public boolean checkInputs() {
		if (txt_IdClient.getText() == null|| txt_IdProdus.getText() == null|| txt_Cantitate.getText() == null|| txt_Adresa.getText() == null) {
			return false;
		} else {
			try {
                Integer.parseInt(txt_IdClient.getText());
                Integer.parseInt(txt_IdProdus.getText());
                Integer.parseInt(txt_Cantitate.getText());
                return true;
            } catch (Exception ex) {
                return false;
            }
		}
	}



	public ArrayList<CumparaClass> getProductList() {
		ArrayList<CumparaClass> productList = new ArrayList<CumparaClass>();
		Connection con = getConnection();
		String query = "SELECT * FROM comanda";

		Statement st;
		ResultSet rs;

		try {
			st = con.createStatement();
			rs = st.executeQuery(query);
			CumparaClass CumparaClass;

			while (rs.next()) {
				CumparaClass = new CumparaClass(rs.getInt("IdComanda"),rs.getInt("idClient"),rs.getInt("IdProdus"), rs.getInt("Cantitate"),rs.getString("Adresa"),rs.getInt("Cost"));
				productList.add(CumparaClass);
			}

		} catch (SQLException ex) {
			Logger.getLogger(ThirdWindow.class.getName()).log(Level.SEVERE, null, ex);
		}
		return productList;
	}
	
	public ArrayList<ClientClass> getProductList2() {
		ArrayList<ClientClass> productList = new ArrayList<ClientClass>();
		Connection con = getConnection();
		String query = "SELECT * FROM client";

		Statement st;
		ResultSet rs;

		try {
			st = con.createStatement();
			rs = st.executeQuery(query);
			ClientClass client;

			while (rs.next()) {
				client = new ClientClass(rs.getInt("idClient"), rs.getString("NumeClient"),rs.getString("Email"), rs.getString("Telefon"));
				productList.add(client);
			}

		} catch (SQLException ex) {
			Logger.getLogger(FirstWindow.class.getName()).log(Level.SEVERE, null, ex);
		}
		return productList;
	}
	
	public ArrayList<ProdusClass> getProductList3() {
		ArrayList<ProdusClass> productList = new ArrayList<ProdusClass>();
		Connection con = getConnection();
		String query = "SELECT * FROM Produs";

		Statement st;
		ResultSet rs;

		try {
			st = con.createStatement();
			rs = st.executeQuery(query);
			ProdusClass Produs;

			while (rs.next()) {
				Produs = new ProdusClass(rs.getInt("idProdus"), rs.getString("NumeProdus"),Integer.parseInt(rs.getString("Pret")),Integer.parseInt(rs.getString("Cantitate")));
				productList.add(Produs);
			}

		} catch (SQLException ex) {
			Logger.getLogger(SecondWindow.class.getName()).log(Level.SEVERE, null, ex);
		}
		return productList;
	}

	public void Show_ThirdWindow_In_JTable() {
		ArrayList<CumparaClass> list = getProductList();
		DefaultTableModel model = (DefaultTableModel) JTable_ThirdWindow.getModel();
		
		ArrayList<ClientClass> list2 = getProductList2();
		
		ArrayList<ProdusClass> list3 = getProductList3();

		model.setRowCount(0);
		
		Object[] row = new Object[8];
		for (int i = 0; i < list.size(); i++) {
			int k=-1;
			row[0] = list.get(i).getIdComanda();
			row[1] = list.get(i).getIdClient();
			
			for (int j = 0; j < list2.size(); j++) {
				int idc=list2.get(j).getIdClient();
				if (idc==(int)row[1])k=j;
			}
			
			if(k==-1)JOptionPane.showMessageDialog(null, "Incorrect Data");
			else row[2] = list2.get(k).getNumeClient();
			row[3] = list.get(i).getIdProdus();
			
			k=-1;
			for (int j = 0; j < list3.size(); j++) {
				int idp=list3.get(j).getIdProdus();
				if (idp==(int)row[3])k=j;
			}
			if(k==-1)JOptionPane.showMessageDialog(null, "Incorrect Data");
			else row[4] = list3.get(k).getNumeProdus();
			
			row[5] = list.get(i).getCantitate();
			int dif=(int)list3.get(k).getCantitate()-(int)row[5];
			if(dif<0) JOptionPane.showMessageDialog(null, "Nu exista destule produse in stoc");
			else{String UpdateQuery = null;
			PreparedStatement ps = null;
			Connection con = getConnection();
			try {
				UpdateQuery = "UPDATE Produs SET Cantitate = ? WHERE idProdus = ?";
				ps = con.prepareStatement(UpdateQuery);
				ps.setInt(1, dif);
				ps.setInt(2, (int)row[3]);
				ps.executeUpdate();
			} catch (SQLException ex) {
				Logger.getLogger(ThirdWindow.class.getName()).log(Level.SEVERE, null, ex);
			}
			}
			row[6] = list.get(i).getAdresa();
			int costTotal=(int)row[5]*(int)list3.get(k).getPret();
			list.get(i).setCost(costTotal);
			row[7] = list.get(i).getCost();
			

			System.out.println(" ");
			System.out.println(" ");
			System.out.println(" ");
			System.out.println(" ");
			System.out.println("--------------------------");
			System.out.println("Comanda numarul : "+row[0]);
			System.out.println("--------------------------");
			System.out.println(" ");
			System.out.println("Clientul:"+ row[2]+ "(" +row[1]+")"+" a cumparat "+row[5]+" de "+row[4]+ "(" +row[3]+")"+" ---- Pret total: "+ row[7]+" RON");
			System.out.println(row[4]+ "(" +row[3]+")"+"Ramasa: "+dif);
			System.out.println(" ");
			System.out.println("--------------------------");
			System.out.println("--------------------------");

			model.addRow(row);
		}
	}

	public void ShowItem(int index) {
		txt_IdComanda.setText(Integer.toString(getProductList().get(index).getIdComanda()));
		txt_IdClient.setText(Integer.toString(getProductList().get(index).getIdClient()));
		txt_IdProdus.setText(Integer.toString(getProductList().get(index).getIdProdus()));
		txt_Cantitate.setText(Integer.toString(getProductList().get(index).getCantitate()));
		txt_Adresa.setText(getProductList().get(index).getAdresa());
	}

	private void initComponents() {

		jFrame1 = new javax.swing.JFrame();
		jFrame2 = new javax.swing.JFrame();
		jPanel1 = new javax.swing.JPanel();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		jLabel4 = new javax.swing.JLabel();
		jLabel5 = new javax.swing.JLabel();
		txt_IdClient = new javax.swing.JTextField();
		txt_IdComanda = new javax.swing.JTextField();
		txt_IdProdus = new javax.swing.JTextField();
		txt_Cantitate = new javax.swing.JTextField();
		txt_Adresa = new javax.swing.JTextField();
		jScrollPane1 = new javax.swing.JScrollPane();
		JTable_ThirdWindow = new javax.swing.JTable();
		Btn_Insert = new javax.swing.JButton();
		Btn_Update = new javax.swing.JButton();
		Btn_Delete = new javax.swing.JButton();

		javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
		jFrame1.getContentPane().setLayout(jFrame1Layout);
		jFrame1Layout.setHorizontalGroup(
				jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 400, Short.MAX_VALUE)
				);
		jFrame1Layout.setVerticalGroup(
				jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 300, Short.MAX_VALUE)
				);


		javax.swing.GroupLayout jFrame2Layout = new javax.swing.GroupLayout(jFrame2.getContentPane());
		jFrame2.getContentPane().setLayout(jFrame2Layout);
		jFrame2Layout.setHorizontalGroup(
				jFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 400, Short.MAX_VALUE)
				);
		jFrame2Layout.setVerticalGroup(
				jFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 300, Short.MAX_VALUE)
				);

		//setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		jPanel1.setBackground(new java.awt.Color(255, 255, 204));

		jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18));
		jLabel1.setText("IDComanda");

		jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18));
		jLabel2.setText("IdClient");
		
		jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18));
		jLabel3.setText("IdProdus");

		jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18));
		jLabel4.setText("Cantitate");
		
		jLabel5.setFont(new java.awt.Font("Tahoma", 1, 18));
		jLabel5.setText("Adresa");


		txt_IdClient.setFont(new java.awt.Font("Tahoma", 1, 14));
		txt_IdClient.setPreferredSize(new java.awt.Dimension(140, 40));
		
		txt_IdProdus.setFont(new java.awt.Font("Tahoma", 1, 14));
		txt_IdProdus.setPreferredSize(new java.awt.Dimension(140, 40));
		
		txt_Cantitate.setFont(new java.awt.Font("Tahoma", 1, 14));
		txt_Cantitate.setPreferredSize(new java.awt.Dimension(140, 40));

		txt_Adresa.setFont(new java.awt.Font("Tahoma", 1, 14));
		txt_Adresa.setPreferredSize(new java.awt.Dimension(140, 40));

		txt_IdComanda.setFont(new java.awt.Font("Tahoma", 1, 14));
		txt_IdComanda.setEnabled(false);
		txt_IdComanda.setPreferredSize(new java.awt.Dimension(59, 40));

		JTable_ThirdWindow.setModel(new javax.swing.table.DefaultTableModel(
				new Object [][] {

				},
				new String [] {
						"IdComanda", "IdClient","NumeClient","IdProdus","NumeProdus","Cantitate","Adresa","Cost"
				}
				));
		JTable_ThirdWindow.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				JTable_ThirdWindowMouseClicked(evt);
			}
		});
		jScrollPane1.setViewportView(JTable_ThirdWindow);


		Btn_Insert.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
		Btn_Insert.setIcon(new javax.swing.ImageIcon(getClass().getResource("/project icon/Add_16x16.png"))); // NOI18N
		Btn_Insert.setText("Insert");
		Btn_Insert.setIconTextGap(15);
		Btn_Insert.setMaximumSize(new java.awt.Dimension(97, 33));
		Btn_Insert.setMinimumSize(new java.awt.Dimension(97, 33));
		Btn_Insert.setPreferredSize(new java.awt.Dimension(97, 33));
		Btn_Insert.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				Btn_InsertActionPerformed(evt);
			}
		});

		Btn_Update.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
		Btn_Update.setIcon(new javax.swing.ImageIcon(getClass().getResource("/project icon/Refresh_16x16.png")));
		Btn_Update.setText("Update");
		Btn_Update.setIconTextGap(15);
		Btn_Update.setMaximumSize(new java.awt.Dimension(97, 33));
		Btn_Update.setMinimumSize(new java.awt.Dimension(97, 33));
		Btn_Update.setPreferredSize(new java.awt.Dimension(97, 33));
		Btn_Update.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				Btn_UpdateActionPerformed(evt);
			}
		});



		Btn_Delete.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
		Btn_Delete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/project icon/Delete_16x16.png")));
		Btn_Delete.setText("Delete");
		Btn_Delete.setIconTextGap(15);
		Btn_Delete.setMaximumSize(new java.awt.Dimension(97, 33));
		Btn_Delete.setMinimumSize(new java.awt.Dimension(97, 33));
		Btn_Delete.setPreferredSize(new java.awt.Dimension(97, 33));
		Btn_Delete.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				Btn_DeleteActionPerformed(evt);
			}
		});



		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(
				jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addGap(36, 36, 36)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
												.addComponent(jLabel1)
												.addComponent(jLabel2)
												.addComponent(jLabel3)
												.addComponent(jLabel4)
												.addComponent(jLabel5)
												)
										.addGroup(jPanel1Layout.createSequentialGroup()
												.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
																.addComponent(txt_IdClient, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addComponent(txt_IdComanda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addComponent(txt_IdProdus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addComponent(txt_Cantitate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addComponent(txt_Adresa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																
																))
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addGap(50, 50, 50))
										.addGap(18, 18, 18)
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

												.addGroup(jPanel1Layout.createSequentialGroup()
														.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
																.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
																.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																		))
														.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addComponent(Btn_Insert, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(5, 5, 5))))
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addGap(160,160, 160)
						.addComponent(Btn_Update, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(Btn_Delete, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						)
				);
		jPanel1Layout.setVerticalGroup(
				jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel1)
												.addComponent(txt_IdComanda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(40, 40, 40)
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel2)
												.addComponent(txt_IdClient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addGap(68, 68, 68)
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel3)
												.addComponent(txt_IdProdus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(40, 40, 40)
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel4)
												.addComponent(txt_Cantitate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(40, 40, 40)
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel5)
												.addComponent(txt_Adresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(40, 40, 40)
										)
								.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 326, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(50, 50, 50)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(Btn_Delete, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(Btn_Insert, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(Btn_Update, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
						.addContainerGap()
						)
				);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				);

		pack();
		setVisible(true);
	}

	private void Btn_InsertActionPerformed(java.awt.event.ActionEvent evt) {

		if (checkInputs()) {
			try {
				Connection con = getConnection();
				PreparedStatement ps = con.prepareStatement("INSERT INTO comanda (IdClient,IdProdus,Cantitate,Adresa) " + "values (?,?,?,?)");
				ps.setString(1, txt_IdClient.getText());
				ps.setString(2, txt_IdProdus.getText());
				ps.setString(3, txt_Cantitate.getText());
				ps.setString(4, txt_Adresa.getText());
				ps.executeUpdate();
				Show_ThirdWindow_In_JTable();
				JOptionPane.showMessageDialog(null, "Data Inserted");
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, ex.getMessage());
			}
		} else {
			JOptionPane.showMessageDialog(null, "One or More Field Are Empty");
		}
	}

	private void Btn_UpdateActionPerformed(java.awt.event.ActionEvent evt) {
		if (checkInputs() && txt_IdComanda.getText() != null) {
			String UpdateQuery = null;
			PreparedStatement ps = null;
			Connection con = getConnection();

			try {
				UpdateQuery = "UPDATE comanda SET IdClient = ? , IdProdus = ? , Cantitate = ? , Adresa = ? WHERE IdComanda = ?";
				ps = con.prepareStatement(UpdateQuery);
				ps.setString(1, txt_IdClient.getText());
				ps.setString(2, txt_IdProdus.getText());
				ps.setString(3, txt_Cantitate.getText());
				ps.setString(4, txt_Adresa.getText());
				ps.setInt(5, Integer.parseInt(txt_IdComanda.getText()));
				ps.executeUpdate();
				Show_ThirdWindow_In_JTable();
				JOptionPane.showMessageDialog(null, "Product Updated");

			} catch (SQLException ex) {
				Logger.getLogger(ThirdWindow.class.getName()).log(Level.SEVERE, null, ex);
			}

		} else {
			JOptionPane.showMessageDialog(null, "One or More Fields Are Empty or Wrong");
		}
	}

	private void Btn_DeleteActionPerformed(java.awt.event.ActionEvent evt) {
		if (!txt_IdComanda.getText().equals("")) {
			try {
				Connection con = getConnection();
				PreparedStatement ps = con.prepareStatement("DELETE FROM comanda WHERE IdComanda=?");
				ps.setInt(1, Integer.parseInt(txt_IdComanda.getText()));
				ps.executeUpdate();
				Show_ThirdWindow_In_JTable();
				JOptionPane.showMessageDialog(null, "Product Deleted");

			} catch (SQLException ex) {
				Logger.getLogger(ThirdWindow.class.getName()).log(Level.SEVERE, null, ex);
				JOptionPane.showMessageDialog(null, "Product Not Deleted");
			}

		} else {
			JOptionPane.showMessageDialog(null, "Product Not Deleted : No IdComanda To Delete");
		}
	}

	private void JTable_ThirdWindowMouseClicked(java.awt.event.MouseEvent evt) {
		int index = JTable_ThirdWindow.getSelectedRow();
		ShowItem(index);
	}

	private javax.swing.JButton Btn_Delete;
	private javax.swing.JButton Btn_Insert;
	private javax.swing.JButton Btn_Update;
	private javax.swing.JTable JTable_ThirdWindow;
	private javax.swing.JFrame jFrame1;
	private javax.swing.JFrame jFrame2;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTextField txt_IdComanda;
	private javax.swing.JTextField txt_IdClient;
	private javax.swing.JTextField txt_IdProdus;
	private javax.swing.JTextField txt_Cantitate;
	private javax.swing.JTextField txt_Adresa;

}
